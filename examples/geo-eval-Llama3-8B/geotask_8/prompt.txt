Given is the single channel tiff file `PH1_CH0.tiff`. Perform an unsupervised segmentation of the homogenous regions of the image.
