from IPython.core.magic import (register_line_magic, register_cell_magic)
from IPython.core.magic_arguments import (argument, magic_arguments, parse_argstring)
from IPython.display import display, Javascript
from chat import SimpleAssitent

assistant  = SimpleAssitent()

@register_cell_magic
@magic_arguments()
#@argument('-c', '--count', action='store_true', help='Count the number of words in the cell')
def chat_magic(line, cell):
    result = assistant.ask(cell)
    code = result.split('```')[-1].split('```')[0]  
    get_ipython().set_next_input(code)
    
def load_ipython_extension(ipython):
    ipython.register_magic_function(chat_magic, 'cell')

print("Chat activated")