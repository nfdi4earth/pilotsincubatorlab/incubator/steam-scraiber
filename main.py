

from autogen import AssistantAgent, UserProxyAgent


llm_config = {"model": "Mistral", "base_url": "http://130.75.51.85:8000", "tags": ["llama", "local"], "api_key":"Bearer no-key"}
assistant = AssistantAgent("assistant", llm_config=llm_config, system_message="Chief technical officer of a tech company")
user_proxy = UserProxyAgent("user_proxy", human_input_mode="NEVER",
    max_consecutive_auto_reply=10,
    is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
    code_execution_config={"work_dir": "tmp_src"},
    llm_config=llm_config,
    system_message="""Reply TERMINATE if the task has been solved at full satisfaction.
Otherwise, reply CONTINUE, or the reason why the task is not solved yet.""")

# Start the chat
user_proxy.initiate_chat(
    assistant,
    message="Create a file with python script that prints `hello world` in terminal. ",
)