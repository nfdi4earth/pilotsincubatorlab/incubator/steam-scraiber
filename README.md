# Steam ScrAIber
![Title image](title-image.png)

The lack of practical skills in the use of relevant programming languages, frameworks, and
libraries prevents the widespread use of automation, especially artificial intelligence (AI)
techniques. This purely practical problem, which can, however, slow down academic progress, is
known in the IT industry as the "developer gap" and refers to the lack of developers with the
appropriate practical know-how. Many technically possible projects can therefore not be
implemented. The problem in the context of academic practice is that experts in the respective
domain can only provide sufficient practical know-how for the use of cutting-edge AI methods with
considerable effort. A good solution to this problem is collaborative work between AI specialists
and domain specialists, but this strategy scales poorly and generates extra costs and slows down
projects. We propose to solve this problem by using automation strategies that have been proven
in IT but also currently offer new possibilities with the emergence of powerful Large Language
Models (LLM). The goal is to implement a tool that has a low entry hurdle and few formalisms, but
still allows the implementation of automated analysis workflows flexibly and quickly. In our case,
we focus on the crystal analysis of volcanic samples. There are already highly automated LLMbased
solutions for application generation (code gen/app gen) and less flexible older software
tools intended for the research domain (Ref. 5 & 6). In this project, state-of-the-art application
generation tools for mineralogical analyses will be adapted and their effectiveness in terms of
quality of results, speed of processing, and learning effort for the user will be investigated. Our
approach is based on three key new developments in recent years. 1. Current models such as
ChatGPT are very effective at automatically generating code based on free text description (Ref.
1). 2. Further development of such models into agents that use interaction with interfaces to
systematically process the task. 3. Combination of automatic code generation and
direct code execution to solve tasks. The examples mentioned are proof of concept and
are not necessarily ready for productive use. The task for us is to find a suitable balance between
automatic generation, (formalised)

## Abstract
A central obstacle to the widespread use of new methods (e.g., machine learning, artificial
intelligence) is of a purely practical nature. Namely, the lack of practical know-how and experience
in the use of relevant programming languages, frameworks and libraries. The programming
landscape is so diverse that even among computer scientists, depending on the domain, the
knowledge of different frameworks and programming languages is unevenly distributed,
depending on the tasks at hand. For specialists from other fields (e.g., Earth Science System),
for whom programming is primarily a means to an end and domain-specific content must be
prioritised, the corresponding know-how for programming is even more sporadically distributed.
To address this problem, we propose the use of Large Language Model assistance systems.
These systems have proven to be very successful in automatic code generation. Combined with
a secure environment where automatic code can be executed directly, they allow users to become
developers and significantly reduce the programming skills required.

# Steam ScrAIber Setup Guide

Follow these steps to set up and run the Steam ScrAIber project on your local machine.

## Prerequisites

Ensure you have the following installed on your system:
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Steps to Set Up
### 1. Clone the Repository
Clone the project repository from GitLab using the following command:

```bash 
git clone https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/steam-scraiber.git
cd steam-scraiber
```


### 2. Build the Docker Containers (optional)
Build the Docker images defined in the docker-compose.yml file:

```bash
docker-compose build 
```

### 3. Run the Docker Containers
Start the services using Docker Compose: 

```bash
docker-compose up 
```

Note: Use the -d flag if you want to run the containers in detached mode: 

```bash
docker-compose up -d 
```

### 4. Access the Application
Once the containers are running, open your web browser and navigate to:

http://localhost:8888

You should now be able to access and interact with the Steam ScrAIber application.

### Stopping the Application
To stop the running containers, press Ctrl+C if running in the foreground, or use: 

```bash
docker-compose down 
```

### Troubleshooting
Ensure no other service is running on port 8888.
Check container logs for errors: 

```bash
docker-compose logs
```

Restart the containers if necessary: 

```bash
docker-compose restart
```
Enjoy using Steam ScrAIber! 


## Outcomes and Trends
1. Open-source code repository published on GitHub of the incubator project aiming at
reproducibility and reusage.
2. Ready to use Docker Compose deployment for the application of the interactive pipelline

## Funding
This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).
