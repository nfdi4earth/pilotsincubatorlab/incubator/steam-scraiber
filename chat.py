from autogen import AssistantAgent, UserProxyAgent

default_llm_config = {"model": "beyonder-4x7b-v3.Q5_K_M.gguf", "cache_seed": None,  "base_url": "http://llama_cpp:8000/v1", "tags": ["llama", "local"], "api_key":"Bearer no-key"}

class SimpleAssitent:

    def __init__(self):
        self.assistant = AssistantAgent("assistant", llm_config=default_llm_config,
                               is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
                               system_message="Chief technical officer of a tech company. Do not repeat yourself. The code will be used in a jupyter notebook cell. Responde with pure python code in single block. No additonal text!"
                               )
        self.user_proxy = UserProxyAgent("user_proxy", human_input_mode="NEVER",
            max_consecutive_auto_reply=0,
            is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
            code_execution_config={"work_dir": '/workdir'},
            llm_config=default_llm_config,
            system_message="""Reply TERMINATE if the task has been solved at full satisfaction. Otherwise, reply CONTINUE, or the reason why the task is not solved yet."""
        )
    
    def ask(self, prompt):
        self.user_proxy.initiate_chat(
            self.assistant,
            message=prompt,
        )
        return self.assistant.last_message()['content'] 