from autogen import AssistantAgent, UserProxyAgent
import pkg_resources
import shutil
import os


distributions = [dist.project_name for dist in pkg_resources.working_set]

eval_path = 'geo-eval/geotask_4'
tmp_path = 'tmp'


def perform_task(task_path, work_dir):
    task_folder = os.path.basename(os.path.normpath(task_path))
    work_path = os.path.join(work_dir, task_folder)
    shutil.copytree(task_path, work_path)
    prompt = open(work_path+'/prompt.txt').read()
    
    #130.75.51.85
    llm_config = {"model": "beyonder-4x7b-v3.Q5_K_M.gguf", "cache_seed": None,  "base_url": "http://llama.cpp:8000/v1", "tags": ["llama", "local"], "api_key":"Bearer no-key"}
    assistant = AssistantAgent("assistant", llm_config=llm_config,
                               is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
                               system_message="Chief technical officer of a tech company. Do not repeat yourself. Save the code to disk in file named `gen.py`"
                               )
    user_proxy = UserProxyAgent("user_proxy", human_input_mode="NEVER",
        max_consecutive_auto_reply=4,
        is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
        code_execution_config={"work_dir": work_path},
        llm_config=llm_config,
        system_message="""Reply TERMINATE if the task has been solved at full satisfaction. Otherwise, reply CONTINUE, or the reason why the task is not solved yet."""
    )

    #prompt = " For the implementation use only libries available in the following list. " + str(distributions) + prompt
    # Start the chat
    user_proxy.initiate_chat(
        assistant,
        message=prompt,
    )

#if os.path.exists(tmp_path):
#    shutil.rmtree(tmp_path)

if not os.path.exists(tmp_path):
    os.makedirs(tmp_path)
perform_task(eval_path, tmp_path)