FROM jupyter/datascience-notebook:latest

#USER root
#RUN apt-get update && apt-get install -y gdal-bin python3-gdal libgdal-dev && gdalinfo --version
#USER jovyan

WORKDIR /src
copy . /src

RUN pip install -r requirements.txt
RUN pip install -e .



WORKDIR /home/jovyan
RUN cp -r /src/geo-eval .

#RUN conda install -c conda-forge gdal
