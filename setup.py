from setuptools import setup

setup(
    name='steam_scraiber',
    version='0.1',
    py_modules=['steam_scraiber'],
    description='A magical chat module for Jupyter',
    author='Artem Leichter',
    author_email=''
)